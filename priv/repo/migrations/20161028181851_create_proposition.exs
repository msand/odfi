defmodule Odfi.Repo.Migrations.CreateProposition do
  use Ecto.Migration

  def change do
    create table(:propositions) do
      add :name, :string
      add :description, :text
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:propositions, [:user_id])

  end
end
