defmodule Odfi.Repo.Migrations.CreateOption do
  use Ecto.Migration

  def change do
    create table(:options) do
      add :name, :string
      add :proposition_id, references(:propositions, on_delete: :nothing)

      timestamps()
    end
    create index(:options, [:proposition_id])

  end
end
