defmodule Odfi.Repo.Migrations.CreateVote do
  use Ecto.Migration

  def change do
    create table(:votes) do
      add :vote, :float
      add :user_id, references(:users, on_delete: :nothing)
      add :proposition_id, references(:propositions, on_delete: :nothing)
      add :option_id, references(:options, on_delete: :nothing)

      timestamps()
    end
    create index(:votes, [:user_id])
    create index(:votes, [:proposition_id])
    create index(:votes, [:option_id])

  end
end
