defmodule Odfi.Router do
  use Odfi.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Odfi do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/users", UserController
    resources "/propositions", PropositionController do
      resources "/options", OptionController
    end
    resources "/votes", VoteController

  end

  # Other scopes may use custom stacks.
  scope "/api", Odfi do
    pipe_through :api
    get "/propositions", PropositionController, :propositions
    get "/results/:id", VoteController, :results
    post "/vote", VoteController, :vote
  end
end
