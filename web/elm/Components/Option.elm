module Option exposing (..)

import Html exposing (Html, span, strong, em, a, text, h3, input, div, br)
import Html.Attributes exposing (class, href, type_, name, min, max, step, value)
import Html.Events exposing (onInput)


type alias Model =
    { id : Int, name : String, value : Maybe String }


type Msg
    = NoOp
    | Slide String String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Slide name value ->
            if name == model.name then
                ( { model | value = Just value }, Cmd.none )
            else
                ( model, Cmd.none )


renderSlider : Model -> Html Msg
renderSlider model =
    let
        val =
            case model.value of
                Just val ->
                    val

                Nothing ->
                    "0"
    in
        div [ class "slider" ]
            [ text "-100%"
            , input [ onInput (Slide model.name), type_ "range", name model.name, Html.Attributes.min "-100", Html.Attributes.max "100", step "any", value val ] []
            , text "100%"
            , br [] []
            , text (val ++ "%")
            ]


view : Model -> Html Msg
view model =
    span [ class "option" ]
        [ h3 [] [ strong [] [ text model.name ] ]
        , renderSlider model
        ]
