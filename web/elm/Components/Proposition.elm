module Components.Proposition exposing (..)

import Json.Decode as Json exposing (list, string, field, map, map2, map3, float, maybe)
import Dict exposing (Dict)
import Json.Encode as JsonE
import Html exposing (Html, text, ul, li, div, h2, span, button)
import Html.Attributes exposing (class, style)
import Html.Events exposing (onClick)
import Http exposing (..)
import Task
import List
import Option
import Debug
import String
import Markdown


type alias Model =
    { id : Int, name : String, description : String, options : List Option.Model, results : Maybe (Dict String Float) }


type alias Id =
    { id : Int }


type Msg
    = Submit String
    | Vote (Result Http.Error (List Id))
    | NewResults (Result Http.Error (Dict String Float))
    | OptionMsg Option.Msg


mapModel : ( Option.Model, Cmd Option.Msg ) -> Option.Model
mapModel ( model, msg ) =
    model


mapMsg : ( Option.Model, Cmd Option.Msg ) -> Cmd Option.Msg
mapMsg ( model, msg ) =
    msg


mapOption : Option.Model -> ( String, JsonE.Value )
mapOption model =
    let
        val =
            case model.value of
                Just val ->
                    val

                _ ->
                    "0"
    in
        ( (toString model.id), JsonE.string val )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OptionMsg optionMsg ->
            let
                updates =
                    List.map (Option.update optionMsg) model.options

                updatedModel =
                    List.map mapModel updates

                cmds =
                    List.map mapMsg updates

                cmd =
                    Cmd.batch cmds
            in
                ( { model | options = updatedModel }, Cmd.map OptionMsg cmd )

        Submit user ->
            ( model
            , postVote
                (JsonE.object
                    [ ( "proposition_id", JsonE.int model.id )
                    , ( "user", JsonE.string user )
                    , ( "votes"
                      , (JsonE.object
                            (List.map mapOption model.options)
                        )
                      )
                    ]
                )
            )

        Vote (Ok ids) ->
            Debug.log "vote success"
                ( model, getResults model )

        Vote (Err error) ->
            (case error of
                BadStatus err ->
                    Debug.log "BadStatus"

                BadPayload code err ->
                    Debug.log "BadPayload"

                BadUrl err ->
                    Debug.log "BadUrl"

                Timeout ->
                    Debug.log "Timeout"

                NetworkError ->
                    Debug.log "NetworkError"
            )
                ( model, Cmd.none )

        NewResults (Ok dict) ->
            ( { model | results = Just dict }, Cmd.none )

        NewResults (Err error) ->
            (case error of
                BadStatus err ->
                    Debug.log "BadStatus"

                BadPayload code err ->
                    Debug.log "BadPayload"

                BadUrl err ->
                    Debug.log "BadUrl"

                Timeout ->
                    Debug.log "Timeout"

                NetworkError ->
                    Debug.log "NetworkError"
            )
                ( model, Cmd.none )


renderOption : Option.Model -> Html Msg
renderOption option =
    li [] [ Html.map OptionMsg (Option.view option) ]


renderOptions : Model -> List (Html Msg)
renderOptions model =
    List.map renderOption model.options


renderResult : Dict String String -> ( String, Float ) -> Html Msg
renderResult idToName ( id, result ) =
    let
        name =
            case Dict.get id idToName of
                Just name ->
                    name

                _ ->
                    id

        width =
            (abs result) / 2

        left =
            if result < 0 then
                50 - width
            else
                50
    in
        div [ style [ ( "width", (toString width) ++ "%" ), ( "left", (toString left) ++ "%" ) ] ] [ text (name ++ " : " ++ (toString (round result))) ]


renderResults : Maybe (Dict String Float) -> Dict String String -> Html Msg
renderResults results idToName =
    (case results of
        Just dict ->
            div [ class "results" ] (List.map (renderResult idToName) (Dict.toList dict))

        _ ->
            div [] []
    )


mapOptionToIdNameTuple : Option.Model -> ( String, String )
mapOptionToIdNameTuple option =
    ( (toString option.id), option.name )


getIdToNameDict : Model -> Dict String String
getIdToNameDict model =
    Dict.fromList (List.map mapOptionToIdNameTuple model.options)


view : String -> Model -> Html Msg
view user model =
    div [ class "proposition" ]
        [ h2 [] [ text model.name ]
        , span [] [ Markdown.toHtml [ class "description" ] model.description ]
        , ul [] (renderOptions model)
        , button [ onClick (Submit user) ] [ text "Vote" ]
        , renderResults model.results (getIdToNameDict model)
        ]


getResults : Model -> Cmd Msg
getResults proposition =
    let
        url =
            "https://serene-anchorage-53639.herokuapp.com/api/results/" ++ (toString proposition.id)

        url2 =
            "http://localhost:4000/api/results/" ++ (toString proposition.id)
    in
        Http.send NewResults <|
            (Http.get url (Json.dict Json.float))


postVote : JsonE.Value -> Cmd Msg
postVote body =
    let
        url =
            "https://serene-anchorage-53639.herokuapp.com/api/vote"

        url2 =
            "http://localhost:4000/api/vote"

        req =
            request
                { method = "POST"
                , headers = []
                , url = url
                , body = (Http.jsonBody body)
                , expect = (expectJson (Json.list (Json.map Id (field "id" Json.int))))
                , timeout = Nothing
                , withCredentials = False
                }
    in
        Http.send Vote req
