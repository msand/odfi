module Components.PropositionList exposing (..)

import Html exposing (Html, text, ul, li, div, h2, button, a, span)
import Html.Attributes exposing (class, href)
import Html.Events exposing (onClick)
import Debug
import Http
import Json.Decode as Json exposing (list, string, field, map2, map3, map5, float, maybe, int, dict)
import Task
import List
import Components.Proposition as Proposition
import Option


type SubPage
    = ListView
    | ShowView Proposition.Model


type alias Model =
    { propositions : List Proposition.Model }


renderProposition : Proposition.Model -> Html Msg
renderProposition proposition =
    li []
        [ div [ class "proposition" ]
            [ h2 [] [ articleLink proposition ]
            , span [] [ text (proposition.description ++ " ") ]
            ]
        ]


renderPropositions : Model -> List (Html Msg)
renderPropositions model =
    List.map renderProposition model.propositions


type Msg
    = Fetch
    | NewPropositions (Result Http.Error (List Proposition.Model))
    | RouteToNewPage SubPage


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Fetch ->
            ( model, getPropositions )

        NewPropositions (Ok newPropositions) ->
            ( { model | propositions = newPropositions }, Cmd.none )

        NewPropositions (Err error) ->
            case error of
                Http.BadPayload code err ->
                    Debug.log "BadPayload"
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        RouteToNewPage page ->
            ( model, Cmd.none )


initialModel : Model
initialModel =
    { propositions = [] }


articleLink : Proposition.Model -> Html Msg
articleLink proposition =
    a
        [ href ("#proposition/" ++ proposition.name ++ "/show")
        , onClick (RouteToNewPage (ShowView proposition))
        ]
        [ text proposition.name ]


view : Model -> Html Msg
view model =
    div [ class "proposition-list" ]
        [ ul [] (renderPropositions model)
        ]



-- HTTP


getPropositions : Cmd Msg
getPropositions =
    let
        url =
            "https://serene-anchorage-53639.herokuapp.com/api/propositions"

        url2 =
            "http://localhost:4000/api/propositions"
    in
        Http.send NewPropositions <| (Http.get url decodePropositions)


decodeOptions : Json.Decoder Option.Model
decodeOptions =
    map3 Option.Model
        (field "id" int)
        (field "name" string)
        (maybe (field "value" string))


decodeProposition : Json.Decoder Proposition.Model
decodeProposition =
    map5 Proposition.Model
        (field "id" int)
        (field "name" string)
        (field "description" string)
        (field "options" (list decodeOptions))
        (maybe (field "results" (dict float)))


decodePropositions : Json.Decoder (List Proposition.Model)
decodePropositions =
    list decodeProposition
