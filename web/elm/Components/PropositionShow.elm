module Components.PropositionShow exposing (..)

import Components.Proposition as Proposition
import Html exposing (..)
import Html.Attributes exposing (href)


view : String -> Proposition.Model -> Html Proposition.Msg
view user model =
    div []
        [ Proposition.view user model
        ]
