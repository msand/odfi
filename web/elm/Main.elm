module Main exposing (..)

import Html exposing (Html, text, div, h1, h2, ul, li, a, input, nav, header, p, span, label)
import Html.Attributes exposing (class, href, placeholder, value, id, for, name, type_)
import Html.Events exposing (onClick, onInput)
import Components.PropositionList as PropositionList
import Components.PropositionShow as PropositionShow
import Components.Proposition as Proposition


-- MODEL


type Page
    = RootView
    | PropositionListView
    | PropositionShowView Proposition.Model


type alias Model =
    { propositionListModel : PropositionList.Model, currentView : Page, user : String }


initialModel : Model
initialModel =
    { propositionListModel = PropositionList.initialModel, currentView = RootView, user = "" }


init : ( Model, Cmd Msg )
init =
    ( initialModel, Cmd.none )



-- UPDATE


type Msg
    = PropositionListMsg PropositionList.Msg
    | UpdateView Page
    | PropositionShowMsg Proposition.Msg
    | Name String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        PropositionListMsg propositionMsg ->
            case propositionMsg of
                PropositionList.RouteToNewPage page ->
                    case page of
                        PropositionList.ShowView proposition ->
                            ( { model | currentView = (PropositionShowView proposition) }, Cmd.map PropositionShowMsg (Proposition.getResults proposition) )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    let
                        ( updatedModel, cmd ) =
                            PropositionList.update propositionMsg model.propositionListModel
                    in
                        ( { model | propositionListModel = updatedModel }, Cmd.map PropositionListMsg cmd )

        UpdateView page ->
            case page of
                PropositionListView ->
                    ( { model | currentView = page }, Cmd.map PropositionListMsg PropositionList.getPropositions )

                _ ->
                    ( { model | currentView = page }, Cmd.none )

        PropositionShowMsg propositionMsg ->
            case model.currentView of
                PropositionShowView proposition ->
                    let
                        ( updatedModel, cmd ) =
                            Proposition.update propositionMsg proposition
                    in
                        ( { model | currentView = (PropositionShowView updatedModel) }, Cmd.map PropositionShowMsg cmd )

                _ ->
                    ( model, Cmd.none )

        Name name ->
            ( { model | user = name }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


mainView : Model -> Html Msg
mainView model =
    let
        loggedIn =
            if model.user == "" then
                span [] []
            else
                li [] [ a [ href "#propositions", onClick (UpdateView PropositionListView) ] [ text "Propositions" ] ]
    in
        div []
            [ header []
                [ h1 [] [ text "OpenDemocracy.fi" ] ]
            , nav [ id "menu" ]
                [ ul []
                    [ li [] [ a [ href "#", onClick (UpdateView RootView) ] [ text "Home" ] ]
                    , loggedIn
                    ]
                ]
            ]


propositionListView : Model -> Html Msg
propositionListView model =
    Html.map PropositionListMsg (PropositionList.view model.propositionListModel)


propositionShowView : Model -> Proposition.Model -> Html Msg
propositionShowView model proposition =
    div [] [ Html.map PropositionShowMsg (PropositionShow.view model.user proposition) ]


welcomeView : Model -> Html Msg
welcomeView model =
    div []
        [ p [] [ text "Welcome to OpenDemocracy.fi!" ]
        , label [ for "user" ] [ text "Choose a username:" ]
        , input [ name "user", id "user", type_ "text", onInput Name, placeholder "User name", value model.user ] []
        ]


pageView : Model -> Html Msg
pageView model =
    case model.currentView of
        RootView ->
            welcomeView model

        PropositionListView ->
            propositionListView model

        PropositionShowView proposition ->
            propositionShowView model proposition


view : Model -> Html Msg
view model =
    div [ class "elm-app" ]
        [ mainView model, div [ class "content" ] [ pageView model ] ]



-- MAIN


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
