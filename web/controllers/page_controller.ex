defmodule Odfi.PageController do
  use Odfi.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
