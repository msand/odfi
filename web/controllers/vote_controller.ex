defmodule Odfi.VoteController do
  use Odfi.Web, :controller

  import Ecto.Query, only: [from: 2]

  alias Odfi.{Vote, User}

  def index(conn, _params) do
    votes = Repo.all(Vote)
    render(conn, "index.html", votes: votes)
  end

  def vote(conn, %{"proposition_id" => proposition_id, "votes" => votes, "user" => user_name}) do
    user = case Repo.get_by(User, %{name: user_name}) do
      nil -> Repo.insert!(User.changeset(%User{}, %{name: user_name, email: user_name, hashed_password: user_name}))
      user -> user
    end
    user_id = user.id
    vote = Enum.map(votes, fn {option_id, vote} ->
      {oid, _} = Integer.parse(option_id)
      changeset =
        Vote.changeset(%Vote{proposition_id: proposition_id, option_id: oid, user_id: user_id}, %{vote: vote})
      Repo.insert!(changeset)
    end)
    render(conn, "index.json", votes: vote)
  end

  def results(conn, %{"id" => id}) do
    voters = Repo.one! from v in Vote, where: v.proposition_id == ^id, select: count(v.user_id, :distinct)

    sums = Repo.all from v in Vote,
      left_join: v2 in Vote,
      on: v.proposition_id == v2.proposition_id and v.option_id == v2.option_id and v.user_id == v2.user_id and v.updated_at < v2.updated_at,
      where: v.proposition_id == ^id and is_nil(v2.updated_at),
      group_by: v.option_id,
      select: {v.option_id, sum(v.vote)}

    normalized = Map.new(Enum.map(sums, fn {id, sum} -> {(Integer.to_string id), sum/voters} end))

    render(conn, "index.json", results: normalized)
  end

  def new(conn, _params) do
    changeset = Vote.changeset(%Vote{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"vote" => vote_params}) do
    changeset = Vote.changeset(%Vote{}, vote_params)

    case Repo.insert(changeset) do
      {:ok, _vote} ->
        conn
        |> put_flash(:info, "Vote created successfully.")
        |> redirect(to: vote_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    vote = Repo.get!(Vote, id)
    render(conn, "show.html", vote: vote)
  end

  def edit(conn, %{"id" => id}) do
    vote = Repo.get!(Vote, id)
    changeset = Vote.changeset(vote)
    render(conn, "edit.html", vote: vote, changeset: changeset)
  end

  def update(conn, %{"id" => id, "vote" => vote_params}) do
    vote = Repo.get!(Vote, id)
    changeset = Vote.changeset(vote, vote_params)

    case Repo.update(changeset) do
      {:ok, vote} ->
        conn
        |> put_flash(:info, "Vote updated successfully.")
        |> redirect(to: vote_path(conn, :show, vote))
      {:error, changeset} ->
        render(conn, "edit.html", vote: vote, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    vote = Repo.get!(Vote, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(vote)

    conn
    |> put_flash(:info, "Vote deleted successfully.")
    |> redirect(to: vote_path(conn, :index))
  end
end
