defmodule Odfi.OptionController do
  use Odfi.Web, :controller

  alias Odfi.Option

  plug :assign_proposition

  def index(conn, _params) do
    options = Repo.all(assoc(conn.assigns[:proposition], :options))
    render(conn, "index.html", options: options)
  end

  def new(conn, _params) do
    changeset =
      conn.assigns[:proposition]
      |> build_assoc(:options)
      |> Option.changeset()
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"option" => option_params}) do
    changeset =
      conn.assigns[:proposition]
      |> build_assoc(:options)
      |> Option.changeset(option_params)

    case Repo.insert(changeset) do
      {:ok, _option} ->
        conn
        |> put_flash(:info, "Option created successfully.")
        |> redirect(to: proposition_option_path(conn, :index, conn.assigns[:proposition]))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    option = Repo.get!(assoc(conn.assigns[:proposition], :options), id)
    render(conn, "show.html", option: option)
  end

  def edit(conn, %{"id" => id}) do
    option = Repo.get!(assoc(conn.assigns[:proposition], :options), id)
    changeset = Option.changeset(option)
    render(conn, "edit.html", option: option, changeset: changeset)
  end

  def update(conn, %{"id" => id, "option" => option_params}) do
    option = Repo.get!(assoc(conn.assigns[:proposition], :options), id)
    changeset = Option.changeset(option, option_params)

    case Repo.update(changeset) do
      {:ok, option} ->
        conn
        |> put_flash(:info, "Option updated successfully.")
        |> redirect(to: proposition_option_path(conn, :show, conn.assigns[:proposition], option))
      {:error, changeset} ->
        render(conn, "edit.html", option: option, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    option = Repo.get!(assoc(conn.assigns[:proposition], :options), id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(option)

    conn
    |> put_flash(:info, "Option deleted successfully.")
    |> redirect(to: proposition_option_path(conn, :index, conn.assigns[:proposition]))
  end

  defp assign_proposition(conn, _opts) do
    case conn.params do
      %{"proposition_id" => proposition_id} ->
        proposition = Repo.get(Odfi.Proposition, proposition_id)
        assign(conn, :proposition, proposition)
      _ ->
        conn
    end
  end
end
