defmodule Odfi.PropositionController do
  use Odfi.Web, :controller

  alias Odfi.Proposition

  def index(conn, _params) do
    propositions = Repo.all(Proposition)
    render(conn, "index.html", propositions: propositions)
  end

  def propositions(conn, _params) do
    propositions = Repo.all from p in Proposition, preload: [:options]
    render(conn, "index.json", propositions: propositions)
  end

  def new(conn, _params) do
    changeset = Proposition.changeset(%Proposition{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"proposition" => proposition_params}) do
    changeset = Proposition.changeset(%Proposition{}, proposition_params)

    case Repo.insert(changeset) do
      {:ok, _proposition} ->
        conn
        |> put_flash(:info, "Proposition created successfully.")
        |> redirect(to: proposition_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    proposition = Repo.get!(Proposition, id)
    render(conn, "show.html", proposition: proposition)
  end

  def edit(conn, %{"id" => id}) do
    proposition = Repo.get!(Proposition, id)
    changeset = Proposition.changeset(proposition)
    render(conn, "edit.html", proposition: proposition, changeset: changeset)
  end

  def update(conn, %{"id" => id, "proposition" => proposition_params}) do
    proposition = Repo.get!(Proposition, id)
    changeset = Proposition.changeset(proposition, proposition_params)

    case Repo.update(changeset) do
      {:ok, proposition} ->
        conn
        |> put_flash(:info, "Proposition updated successfully.")
        |> redirect(to: proposition_path(conn, :show, proposition))
      {:error, changeset} ->
        render(conn, "edit.html", proposition: proposition, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    proposition = Repo.get!(Proposition, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(proposition)

    conn
    |> put_flash(:info, "Proposition deleted successfully.")
    |> redirect(to: proposition_path(conn, :index))
  end
end
