defmodule Odfi.Proposition do
  use Odfi.Web, :model

  @derive {Poison.Encoder, only: [:name, :description, :options, :votes]}
  schema "propositions" do
    field :name, :string
    field :description, :string
    has_many :options, Odfi.Option
    has_many :votes, Odfi.Vote
    belongs_to :user, Odfi.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description])
    |> validate_required([:name, :description])
  end
end
