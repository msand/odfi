defmodule Odfi.User do
  use Odfi.Web, :model

  schema "users" do
    field :email, :string
    field :name, :string
    field :hashed_password, :string
    has_many :propositions, Odfi.Proposition
    has_many :votes, Odfi.Vote

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :name, :hashed_password])
    |> validate_required([:email, :name, :hashed_password])
  end
end
