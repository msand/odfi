defmodule Odfi.Option do
  use Odfi.Web, :model

  @derive {Poison.Encoder, only: [:name]}
  schema "options" do
    field :name, :string
    belongs_to :proposition, Odfi.Proposition
    has_many :votes, Odfi.Vote

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :proposition_id])
    |> foreign_key_constraint(:proposition_id)
    |> validate_required([:name, :proposition_id])
  end
end
