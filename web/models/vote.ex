defmodule Odfi.Vote do
  use Odfi.Web, :model

  @derive {Poison.Encoder, except: [:__meta__, :option, :proposition, :user]}
  schema "votes" do
    field :vote, :float
    belongs_to :user, Odfi.User
    belongs_to :proposition, Odfi.Proposition
    belongs_to :option, Odfi.Option

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:vote, :proposition_id, :option_id, :user_id])
    |> foreign_key_constraint(:proposition_id)
    |> foreign_key_constraint(:option_id)
    |> foreign_key_constraint(:user_id)
    |> validate_required([:vote, :proposition_id, :option_id, :user_id])
  end
end
