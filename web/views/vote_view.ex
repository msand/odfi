defmodule Odfi.VoteView do
  use Odfi.Web, :view

  def render("index.json", %{votes: votes}) do
    votes
  end

  def render("index.json", %{results: results}) do
    results
  end
end
