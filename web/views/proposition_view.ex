defmodule Odfi.PropositionView do
  use Odfi.Web, :view

  def render("index.json", %{propositions: propositions}) do
    Enum.map(propositions, fn proposition -> %{
      id: proposition.id,
      name: proposition.name,
      description: proposition.description,
      options: Enum.map(proposition.options, fn option -> %{id: option.id, name: option.name} end)
    } end )
  end
end
