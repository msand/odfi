# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :odfi,
  ecto_repos: [Odfi.Repo]

# Configures the endpoint
config :odfi, Odfi.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Ss+viMGEXmairslO20dWBs3zx19CMh/HOpYL7mIqn0vcuQV+ilf+aUwCKZNQqw9o",
  render_errors: [view: Odfi.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Odfi.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
