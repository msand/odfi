defmodule Odfi.PropositionTest do
  use Odfi.ModelCase

  alias Odfi.Proposition

  @valid_attrs %{description: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Proposition.changeset(%Proposition{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Proposition.changeset(%Proposition{}, @invalid_attrs)
    refute changeset.valid?
  end
end
