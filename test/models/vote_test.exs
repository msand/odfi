defmodule Odfi.VoteTest do
  use Odfi.ModelCase

  alias Odfi.Vote

  @valid_attrs %{vote: "120.5"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Vote.changeset(%Vote{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Vote.changeset(%Vote{}, @invalid_attrs)
    refute changeset.valid?
  end
end
