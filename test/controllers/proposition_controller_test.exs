defmodule Odfi.PropositionControllerTest do
  use Odfi.ConnCase

  alias Odfi.Proposition
  @valid_attrs %{description: "some content", name: "some content"}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, proposition_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing propositions"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, proposition_path(conn, :new)
    assert html_response(conn, 200) =~ "New proposition"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, proposition_path(conn, :create), proposition: @valid_attrs
    assert redirected_to(conn) == proposition_path(conn, :index)
    assert Repo.get_by(Proposition, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, proposition_path(conn, :create), proposition: @invalid_attrs
    assert html_response(conn, 200) =~ "New proposition"
  end

  test "shows chosen resource", %{conn: conn} do
    proposition = Repo.insert! %Proposition{}
    conn = get conn, proposition_path(conn, :show, proposition)
    assert html_response(conn, 200) =~ "Show proposition"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, proposition_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    proposition = Repo.insert! %Proposition{}
    conn = get conn, proposition_path(conn, :edit, proposition)
    assert html_response(conn, 200) =~ "Edit proposition"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    proposition = Repo.insert! %Proposition{}
    conn = put conn, proposition_path(conn, :update, proposition), proposition: @valid_attrs
    assert redirected_to(conn) == proposition_path(conn, :show, proposition)
    assert Repo.get_by(Proposition, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    proposition = Repo.insert! %Proposition{}
    conn = put conn, proposition_path(conn, :update, proposition), proposition: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit proposition"
  end

  test "deletes chosen resource", %{conn: conn} do
    proposition = Repo.insert! %Proposition{}
    conn = delete conn, proposition_path(conn, :delete, proposition)
    assert redirected_to(conn) == proposition_path(conn, :index)
    refute Repo.get(Proposition, proposition.id)
  end
end
